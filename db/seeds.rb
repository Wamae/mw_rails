# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
ActiveRecord::Base.transaction do
#Grid types
  GridType.create(name: "Flat Grid")
  GridType.create(name: "Flat List")
  GridType.create(name: "Card Grid")
  GridType.create(name: "Card List")
  GridType.create(name: "Magic Grid")

#Disc types
  DiscType.create(name: "Square Disc")
  DiscType.create(name: "Circle Disc")
  DiscType.create(name: "Rotate Disc")

#Background modes
  BackgroundMode.create(id: 0, name: "Just ActionBar")
  BackgroundMode.create(id: 1, name: "Full Background")

#App types
  AppType.create!(name: "Single Radio")
  AppType.create!(name: "Multi Radios")

#Android UIs
  AndroidUi.create(name: "Top Chart", grid_type_id: 4)
  AndroidUi.create(name: "Genres", grid_type_id: 3)
  AndroidUi.create(name: "Favourites", grid_type_id: 1)
  AndroidUi.create(name: "Themes", grid_type_id: 3)
  AndroidUi.create(name: "Genre Details", grid_type_id: 4)
  AndroidUi.create(name: "Search", grid_type_id: 4)

#Configs
  RemoteConfig.create(background_mode_id: 0, disc_type_id: 3, app_type_id: 2)

#ConfigAndoirUI
  RemoteConfigAndroidUi.create(remote_config_id: 1, android_ui_id: 1)
  RemoteConfigAndroidUi.create(remote_config_id: 1, android_ui_id: 2)
  RemoteConfigAndroidUi.create(remote_config_id: 1, android_ui_id: 3)
  RemoteConfigAndroidUi.create(remote_config_id: 1, android_ui_id: 4)
  RemoteConfigAndroidUi.create(remote_config_id: 1, android_ui_id: 5)
  RemoteConfigAndroidUi.create(remote_config_id: 1, android_ui_id: 6)

#Genres
  Genre.create!(
      [
          {
              "name": "General",
              "img": "/uploads/genres/genre_37941_1024px-Information_icon_with_gradient_background.svg.png"
          },
          {
              "name": "Islam",
              "img": "/uploads/genres/genre_225_images (1).jpeg"
          },
          {
              "name": "Christian",
              "img": ""
          },
          {
              "name": "Pop",
              "img": "/uploads/genres/genre_94764_pop.jpg"
          },
          {
              "name": "RnB Soul",
              "img": "genre_44634_rnb.jpg"
          },
          {
              "name": "Gospel",
              "img": "/uploads/genres/genre_63332_gospel.jpg"
          },
          {
              "name": "Country",
              "img": "/uploads/genres/genre_32178_country.jpg"
          },
          {
              "name": "Rock",
              "img": "/uploads/genres/genre_79828_rock.jpg"
          },
          {
              "name": "Jazz",
              "img": "/uploads/genres/genre_88902_jazz.jpg"
          },
          {
              "name": "Deep House",
              "img": "/uploads/genres/genre_61591_deephouse.jpg"
          },
          {
              "name": "Dubstep",
              "img": "/uploads/genres/genre_86053_dubstep.jpg"
          },
          {
              "name": "Ambient",
              "img": "/uploads/genres/genre_94121_ambient.jpeg"
          },
          {
              "name": "Rap HipHop",
              "img": "/uploads/genres/genre_17511_hiphop.jpg"
          }
      ]
  )

#Radios
  Radio.create!(
      [
          {
              "name": "Capital Radio Malawi - FM 102.5 - Blantyre",
              "genre_id": 2,
              "img": "/uploads/radios/radio_87362_capital_fm_mw.png",
              "bitrate": 1,
              "tags": "ADULT CONTEMPORARY | HIP HOP | ROCK | TOP 40 ",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://cdn.voscast.com/resources/?key=5eb8e4e0cc7215ccdd4a06cf9449f77b&c=wmp",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Classic 105 - FM 105.2 - Nairobi",
              "genre_id": 2,
              "img": "/uploads/radios/radio_61694_classic_105_ke.jpeg",
              "bitrate": 128,
              "tags": "CLASSIC ROCK | HITS | NEWS | POP | RNB | TALK",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://streams.radioafricagroup.co.ke:88/broadwave.mp3?src=5&kbps=128",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Clouds FM - FM 88.5 - Dar es Salaam",
              "genre_id": 2,
              "img": "/uploads/radios/radio_13689_cloud_fm_tz.jpeg",
              "bitrate": 1,
              "tags": "HITS | MUSIC | POP | ROCK | TOP 40",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://eu6.fastcast4u.com:5306/listen.pls",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "East Africa Radio - FM 88.1 - Dar es Salaam",
              "genre_id": 2,
              "img": "/uploads/radios/radio_84127_east_africa_radio_tz.png",
              "bitrate": 64,
              "tags": "BONGO FLAVA | HIP HOP  |TOP 40 | URBAN",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://www.ophanim.net:8270/listen.pls",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Hope FM - FM 93.3 - Nairobi",
              "genre_id": 2,
              "img": "/uploads/radios/radio_97746_hope_fm.jpeg",
              "bitrate": 1,
              "tags": "CHRISTIAN | EDUCATION | PRAISE & WORSHIP | TALK",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://50.7.99.155:12087/listen.pls?sid=1",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Kameme FM - FM 101.1 - Nairobi",
              "genre_id": 2,
              "img": "/uploads/radios/radio_38912_kameme_fm.png",
              "bitrate": 1,
              "tags": "CULTURE | GOSPEL | NEWS | TALK",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://stream.zenolive.com/wxn43ghqtceuv",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "KBC English - FM 95.6 - Nairobi",
              "genre_id": 2,
              "img": "/uploads/radios/radio_21152_kcb_kenya.jpeg",
              "bitrate": 1,
              "tags": "EDUCATION | INFORMATION | NEWS | TALK",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://91.121.165.88:8116/listen.pls?sid=1",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Kiss FM",
              "genre_id": 2,
              "img": "/uploads/radios/radio_22041_kiss_fm.png",
              "bitrate": 1,
              "tags": "ADULT CONTEMPORARY | HIP HOP | NEWS | POP | REGGAE | TALK | TOP 40",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://streams.radioafricagroup.co.ke:88/broadwave.mp3",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Milele FM - FM 93.6 - Nairobi",
              "genre_id": 2,
              "img": "/uploads/radios/radio_77127_milele_fm_ke.jpeg",
              "bitrate": 1,
              "tags": "ADULT CONTEMPORARY | HITS MUSIC | POP | RNB | ROCK",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://stream.zenolive.com/ur6rs3kqtceuv",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "NRG Radio - FM 91.3 - Nairobi",
              "genre_id": 2,
              "img": "/uploads/radios/radio_62543_nrg_fm.jpeg",
              "bitrate": 1,
              "tags": "ADULT CONTEMPORARY | AFRICAN DANCE | EDM | GOSPEL | URBAN",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://net1.citrus3.com:8552/listen.pls?sid=1",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Power FM 101",
              "genre_id": 2,
              "img": "/uploads/radios/radio_31842_power_fm.jpeg",
              "bitrate": 1,
              "tags": "MUSIC | SPORTING EVENTS",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://50.7.71.219:9699/stream/",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Radio Islam Malawi - Limbe",
              "genre_id": 2,
              "img": "/uploads/radios/radio_60647_radio_islam_malawi.jpeg",
              "bitrate": 1,
              "tags": "EDUCATION | ISLAMIC | RELIGIOUS",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://178.obj.netromedia.net:8028/listen.pls",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Radio Maria Malawi - FM 99.2 - Blantyre",
              "genre_id": 2,
              "img": "/uploads/radios/radio_15095_rasio_maria_malawi.jpeg",
              "bitrate": 1,
              "tags": "CHRISTIAN | EDUCATION | RELIGIOUS | TALK",
              "type_radio": "AAC",
              "source_radio": "Shoutcast",
              "link_radio": "http://50.7.181.186:8040/listen.pls",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Radio Maria Tanzania - FM 89.1 - Dar es Salaam",
              "genre_id": 2,
              "img": "/uploads/radios/radio_10414_radio_maria_tz.jpeg",
              "bitrate": 1,
              "tags": "CATHOLIC | CHRISTIAN | RELIGIOUS",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://dreamsiteradiocp2.com:8034/listen.pls",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "RadioOne - FM 89.5 - Dar es Salaam",
              "genre_id": 2,
              "img": "/uploads/radios/radio_20691_radio_one_fm_tz.png",
              "bitrate": 64,
              "tags": "AFRICAN | BONGO FLAVA  | FOLK | VARIETY",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://78.129.232.226:8085/listen.pls?sid=1",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Safina Radio - FM 92.6 - Arusha",
              "genre_id": 2,
              "img": "/uploads/radios/radio_62427_safina_radio_tz.jpeg",
              "bitrate": 1,
              "tags": "AFRICAN | ENTERTAINMENT",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://s14.myradiostream.com:7838/listen.pls",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Times Radio Malawi - FM 92.8 - Blantyre",
              "genre_id": 2,
              "img": "/uploads/radios/radio_7939_times_mw.jpeg",
              "bitrate": 1,
              "tags": "Malawi",
              "type_radio": "MP3",
              "source_radio": "Other",
              "link_radio": "http://ice31.securenetsystems.net/TIMESFM?type=.flv",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Voice of Hope FM - FM 90.5 - Nelspruit",
              "genre_id": 2,
              "img": "/uploads/radios/radio_63210_voice_of_hope.jpeg",
              "bitrate": 1,
              "tags": "AFRICAN GOSPEL | CHRISTIAN | CHRISTIAN | ROCK | GOSPEL",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://zas3.ndx.co.za/proxy/voiceofhope?mp=/stream",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": ""
          },
          {
              "name": "Zodiak Broadcasting Station - FM 95.1 - Lilongwe",
              "genre_id": 2,
              "img": "/uploads/radios/radio_62967_zodiak.jpeg",
              "bitrate": 1,
              "tags": "Malawi",
              "type_radio": "MP3",
              "source_radio": "Shoutcast",
              "link_radio": "http://ice31.securenetsystems.net/0079?type=.flv",
              "user_agent_radio": "",
              "url_facebook": "",
              "url_twitter": "",
              "url_instagram": "",
              "url_website": "http://zodiakmalawi.com/"
          }
      ])

#AppSettings
  AppSetting.create!(
      name: "Maison de Wamae",
      email: "dev@maisondewamae.com",
      copyright: "Maison de Wamae",
      phone: "+254716690166",
      website: "http://maisondewamae.com",
      facebook: "http://maisondewamae.com",
      twitter: "http://maisondewamae.com",
      terms_of_use: IO.read("./public/terms_of_use.html"),
      privacy_policy: IO.read("./public/privacy_policy.html")
  )

end
