# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_24_102634) do

  create_table "android_uis", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.bigint "grid_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["grid_type_id"], name: "index_android_uis_on_grid_type_id"
  end

  create_table "app_settings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "copyright"
    t.string "phone"
    t.string "website"
    t.string "facebook"
    t.string "twitter"
    t.text "terms_of_use"
    t.text "privacy_policy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "app_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "background_modes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disc_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "img"
    t.string "description"
    t.boolean "is_active", default: true
  end

  create_table "grid_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "radios", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.text "tags"
    t.string "bitrate"
    t.string "img"
    t.string "type_radio"
    t.string "source_radio"
    t.string "link_radio"
    t.string "user_agent_radio"
    t.string "url_facebook"
    t.string "url_twitter"
    t.string "url_instagram"
    t.string "url_website"
    t.boolean "is_featured", default: true
    t.boolean "is_active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "genre_id"
  end

  create_table "remote_config_android_uis", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "remote_config_id"
    t.bigint "android_ui_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["android_ui_id"], name: "index_remote_config_android_uis_on_android_ui_id"
    t.index ["remote_config_id"], name: "index_remote_config_android_uis_on_remote_config_id"
  end

  create_table "remote_configs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "background_mode_id", default: 0
    t.bigint "disc_type_id", default: 1
    t.bigint "app_type_id", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["app_type_id"], name: "index_remote_configs_on_app_type_id"
    t.index ["background_mode_id"], name: "index_remote_configs_on_background_mode_id"
    t.index ["disc_type_id"], name: "index_remote_configs_on_disc_type_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "api_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

end
