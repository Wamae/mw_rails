class DiscType < ActiveRecord::Migration[5.2]
  def change
    create_table :disc_types do |t|
      t.string :name
      t.timestamps
    end
  end
end
