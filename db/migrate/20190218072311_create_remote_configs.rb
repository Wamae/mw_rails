class CreateRemoteConfigs < ActiveRecord::Migration[5.2]
  def change
    create_table :remote_configs do |t|
      t.references :background_mode, default: 0
      t.references :disc_type, default: 1
      t.references :app_type, default: 1
      t.timestamps
    end
  end
end
