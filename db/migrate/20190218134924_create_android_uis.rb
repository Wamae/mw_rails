class CreateAndroidUis < ActiveRecord::Migration[5.2]
  def change
    create_table :android_uis do |t|
      t.string :name
      t.references :grid_type
      t.timestamps
    end
  end
end
