class AddGenreInRadio < ActiveRecord::Migration[5.2]
  def change
    add_column :radios, :genre_id, :string
  end
end
