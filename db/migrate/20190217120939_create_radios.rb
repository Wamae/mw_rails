class CreateRadios < ActiveRecord::Migration[5.2]
  def change
    create_table :radios do |t|
      t.string :name
      t.text :tags
      t.string :bitrate
      t.string :img
      t.string :type_radio
      t.string :source_radio
      t.string :link_radio
      t.string :user_agent_radio
      t.string :url_facebook
      t.string :url_twitter
      t.string :url_instagram
      t.string :url_website
      t.boolean :is_featured, default: true
      t.boolean :is_active, default: true

      t.timestamps
    end
  end
end
