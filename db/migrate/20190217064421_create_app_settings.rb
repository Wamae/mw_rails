class CreateAppSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :app_settings do |t|
      t.string :name
      t.string :email
      t.string :copyright
      t.string :phone
      t.string :website
      t.string :facebook
      t.string :twitter
      t.text :terms_of_use
      t.text :privacy_policy

      t.timestamps
    end
  end
end
