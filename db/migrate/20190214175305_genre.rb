class Genre < ActiveRecord::Migration[5.2]
  def change
    create_table :genres do |t|
      t.string :name
      t.string :img
      t.string :description
      t.boolean :is_active, default: true
    end
  end
end
