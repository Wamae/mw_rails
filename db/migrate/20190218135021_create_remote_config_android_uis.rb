class CreateRemoteConfigAndroidUis < ActiveRecord::Migration[5.2]
  def change
    create_table :remote_config_android_uis do |t|
      t.references :remote_config
      t.references :android_ui
      t.timestamps
    end
  end
end
