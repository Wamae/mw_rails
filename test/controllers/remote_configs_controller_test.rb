require 'test_helper'

class RemoteConfigsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @remote_config = remote_configs(:one)
  end

  test "should get index" do
    get remote_configs_url
    assert_response :success
  end

  test "should get new" do
    get new_remote_config_url
    assert_response :success
  end

  test "should create remote config" do
    assert_difference('RemoteConfig.count') do
      post remote_configs_url, params: {remote_config: {}}
    end

    assert_redirected_to remote_config_url(RemoteConfig.last)
  end

  test "should show remote config" do
    get remote_config_url(@remote_config)
    assert_response :success
  end

  test "should get edit" do
    get edit_remote_config_url(@remote_config)
    assert_response :success
  end

  test "should update remote config" do
    patch remote_config_url(@remote_config), params: {app_remote_config: {}}
    assert_redirected_to remote_config_url(@remote_config)
  end

  test "should destroy remote config" do
    assert_difference('RemoteConfig.count', -1) do
      delete remote_config_url(@remote_config)
    end

    assert_redirected_to remote_configs_url
  end
end
