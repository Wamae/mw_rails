json.extract! remote_config, :id, :created_at, :updated_at
json.url remote_config_url(remote_config, format: :json)
