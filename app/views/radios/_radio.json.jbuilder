json.extract! radio, :id, :created_at, :updated_at
json.url radio_url(radio, format: :json)
