json.extract! app_setting, :id, :created_at, :updated_at
json.url app_setting_url(app_setting, format: :json)
