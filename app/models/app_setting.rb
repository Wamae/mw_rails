class AppSetting < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true
  validates :copyright, presence: true
  validates :phone, presence: true
  validates :website, presence: true
  validates :facebook, presence: true
  validates :twitter, presence: true
  validates :terms_of_use, presence: true
  validates :privacy_policy, presence: true
end
