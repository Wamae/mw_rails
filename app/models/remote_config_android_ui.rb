class RemoteConfigAndroidUi < ApplicationRecord
  # self.primary_keys = :android_ui_id, :config_id
  belongs_to :android_ui
  belongs_to :remote_config
  accepts_nested_attributes_for :remote_config, :android_ui
end
