class AndroidUi < ApplicationRecord
  TOP_CHART = 1
  GENRES = 2
  FAVOURITES = 3
  THEMES = 4
  GENRE_DETAILS = 5
  SEARCH = 6

  validates :name, presence: true, uniqueness: true
  belongs_to :grid_type
  has_many :remote_config_android_uis
  has_many :remote_configs, through: :remote_config_android_uis
  # accepts_nested_attributes_for :configs
  # accepts_nested_attributes_for :config_android_uis, :allow_destroy => true
end
