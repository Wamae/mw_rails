class Radio < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :genre_id, presence: true
  validates :link_radio, presence: true
  validates :tags, presence: true

  belongs_to :genre

  acts_as_api

  api_accessible :radios do |template|
    template.add :id
    template.add :name
    template.add :img
    template.add :bitrate
    template.add :tags
    template.add :type_radio
    template.add :source_radio
    template.add :link_radio
    template.add :user_agent_radio
    template.add :url_facebook
    template.add :url_twitter
    template.add :url_instagram
    template.add :url_website
  end
end
