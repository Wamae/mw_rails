class Genre < ApplicationRecord
  validates :name, presence: true
  has_one :radio

  acts_as_api
  api_accessible :genres do |template|
    template.add :id
    template.add :name
    template.add :img
  end
end
