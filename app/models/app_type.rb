class AppType < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_one :remote_config
end
