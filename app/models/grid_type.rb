class GridType < ApplicationRecord
  MAGIC_GRID = 5

  validates :name, presence: true, uniqueness: true
  has_one :android_ui
end
