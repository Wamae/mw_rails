class RemoteConfig < ApplicationRecord
  belongs_to :app_type
  belongs_to :disc_type
  belongs_to :background_mode
  has_many :remote_config_android_uis, inverse_of: :remote_config
  has_many :android_uis, through: :remote_config_android_uis
  accepts_nested_attributes_for :android_uis
  accepts_nested_attributes_for :remote_config_android_uis, :allow_destroy => true

  acts_as_api
  api_accessible :remote_configs do |template|
    template.add :id
    template.add :background_mode_id
    template.add :ui_top_chart
    template.add :ui_genre
    template.add :ui_favorite
    template.add :ui_themes
    template.add :ui_detail_genre
    template.add :ui_player
    template.add :ui_search
    template.add :app_type_id
  end

  def ui_top_chart
    self.android_uis.find(1).grid_type_id
  end

  def ui_genre
    self.android_uis.find(2).grid_type_id
  end

  def ui_favorite
    self.android_uis.find(3).grid_type_id
  end

  def ui_themes
    self.android_uis.find(4).grid_type_id
  end

  def ui_detail_genre
    self.android_uis.find(5).grid_type_id
  end

  def ui_player
    self.disc_type_id
  end

  def ui_search
    self.android_uis.find(6).grid_type_id
  end

end
