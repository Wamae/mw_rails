class Api::Private::V1::AuthController < Api::Private::V1::BaseController
  # include HooksHelper
  before_action :authenticate_user, :except => [:sign_in]

  def sign_in
    user_email = params['email']
    user_pass = params['password']

    if user_email && user_pass
      user = User.find_by(email: user_email)

      if user and user.valid_password?(user_pass)

        respond_to do |format|
          format.json do
            render json: {
                status: 1, message: "Successful login",
                auth_token: user.api_token
            }.to_json
          end
        end
      else
        respond_to do |format|
          format.json do
            render :json => {:status => 0, :message => "Wrong username/password"}.to_json
          end
        end
      end
    else
      respond_to do |format|
        format.json do
          render :json => {:status => 0, :message => "Wrong username/password", data: nil}.to_json
        end
      end
    end
  end

  def sign_out
    sign_out
  end
end