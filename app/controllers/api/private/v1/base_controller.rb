class Api::Private::V1::BaseController < ActionController::Base
  skip_before_action :verify_authenticity_token
  before_action :authenticate_with_token
  # before_action :authenticate_user
  self.responder = ActsAsApi::Responder
  respond_to :json, :xml

  def current_user
    @current_user
  end

  private

  def authenticate_with_token
    if params[:api_token]
      @current_user = User.find_by_api_token(params[:api_token])
      if @current_user
        sign_in(@current_user)
      else
        respond_with({:status => 0, :message => "Unauthorized access. Invalid token"})
      end
    end
  end

  def authenticate_user
    user_email = params[:api_token].presence
    user = user_email && User.find_by(api_token: user_email, active: true, deleted: false)

    @current_user = user if user && Devise.secure_compare(user.authentication_token, params[:token])

    unless @current_user && @current_company
      if @current_user.nil?
        respond_with({:status => 0, :message => "Unauthorized access. Invalid token", :status => 200})
      elsif !@current_user.active_for_authentication?
        respond_with({:status => 0, :message => "User not authorized.", :status => 200})
      else
        respond_with({:status => 0, :message => "Unauthorized access. Invalid api key", :status => 200})
      end
    end

  end

end