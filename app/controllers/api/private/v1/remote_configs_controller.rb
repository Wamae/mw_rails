class Api::Private::V1::RemoteConfigsController < Api::Private::V1::BaseController
  def index
    @remote_configs = RemoteConfig.all
    respond_to do |format|
      format.json {render_for_api :remote_configs, json: @remote_configs, root: :datas, meta: {status: 200, msg: "success"}}
    end
  end
end