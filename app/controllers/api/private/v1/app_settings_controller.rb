class Api::Private::V1::AppSettingsController < Api::Private::V1::BaseController
  def index
    @app_settings = AppSetting.all
    respond_to do |format|
      format.json {render_for_api :app_settings, json: @app_settings, root: :datas}
    end
  end
end