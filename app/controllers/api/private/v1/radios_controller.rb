class Api::Private::V1::RadiosController < Api::Private::V1::BaseController
  def index
    if params[:genre_id]
      @radios = Radio.where(genre_id: params[:genre_id])
    else
      @radios = Radio.all
    end

    respond_to do |format|
      format.json {render_for_api :radios, json: @radios, root: :datas, meta: {status: 200, msg: "success"}}
    end
  end

  def show
    @radio = Radio.find(params[:id])
    respond_to do |format|
      format.json {render_for_api :radios, json: @radio, root: :datas, meta: {status: 200, msg: "success"}}
    end
  end

  def search_by_name
    @radio = Radio.where("name LIKE '%#{params[:q]}%'")
    respond_to do |format|
      format.json {render_for_api :radios, json: @radio, root: :datas, meta: {status: 200, msg: "success"}}
    end
  end

end