class Api::Private::V1::GenresController < Api::Private::V1::BaseController
  def index
    @genres = Genre.all
    respond_to do |format|
      format.json {render_for_api :genres, json: @genres, root: :datas, meta: {status: 200, msg: "success"}}
    end
  end
end