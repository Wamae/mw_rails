class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def set_titles(parent_title, title)
    @parent_title = parent_title
    @title = title
  end

  def upload_file(upload_path, file_name, uploaded_io)
    File.open(Rails.root.join('public', upload_path, file_name), 'wb') do |file|
      file.write(uploaded_io.read)
    end
  end

end
