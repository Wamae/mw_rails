class GenresController < ApplicationController
  before_action {set_titles("General Settings", "Genres")}
  before_action :set_genre, only: [:show, :edit, :update, :destroy]

  # GET /genres
  # GET /genres.json
  def index
    @sub_title = "All Genres"
    @new_link = view_context.link_to "New Genre", new_genre_path, class: "dropdown-item"
    @genres = Genre.all
  end

  # GET /genres/1
  # GET /genres/1.json
  def show
  end

  # GET /genres/new
  def new
    @sub_title = "New Genre"
    @genre = Genre.new
  end

  # GET /genres/1/edit
  def edit
    @sub_title = "Editing Genre"
  end

  # POST /genres
  # POST /genres.json
  def create
    @genre = Genre.new(genre_params)

    respond_to do |format|


      uploaded_io = params[:genre][:img]
      if uploaded_io
        file_name = SecureRandom.uuid
        @genre.img = "/uploads/genres/#{file_name}"
      end

      if @genre.save

        if uploaded_io
          upload_file("uploads/genres", file_name, uploaded_io)
        end

        format.html {
          flash[:success] = 'Genre was successfully created.'
          redirect_to @genre
        }
        format.json {render :show, status: :created, location: @genre}
      else
        format.html {render :new}
        format.json {render json: @genre.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /genres/1
  # PATCH/PUT /genres/1.json
  def update
    respond_to do |format|

      uploaded_io = params[:genre][:img]
      if uploaded_io
        file_name = SecureRandom.uuid
        @genre.img = "/uploads/genres/#{file_name}"
      end

      if @genre.update(genre_params)

        if uploaded_io
          upload_file("uploads/genres", file_name, uploaded_io)
        end

        format.html {
          flash[:success] = 'Genre was successfully updated.'
          redirect_to @genre
        }
        format.json {render :show, status: :ok, location: @genre}
      else
        format.html {render :edit}
        format.json {render json: @genre.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /genres/1
  # DELETE /genres/1.json
  def destroy
    @genre.destroy
    respond_to do |format|
      format.html {redirect_to genres_url, notice: 'Genre was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_genre
    @genre = Genre.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def genre_params
    params.require(:genre).permit(:name, :description, :is_active)
  end
end
