class RemoteConfigsController < ApplicationController
  before_action {set_titles("General Settings", "Remote configs")}
  before_action :set_remote_config, only: [:show, :edit, :update, :destroy]
  # skip_before_action :verify_authenticity_token

  # GET /remote_configs
  # GET /remote_configs.json
  def index
    @sub_title = "All Remote Configs"
    @remote_configs = RemoteConfig.all
  end

  # GET /remote_configs/1
  # GET /remote_configs/1.json
  def show
  end

  # GET /remote_configs/new
  def new
    @sub_title = "New Remote Config"
    @remote_config = RemoteConfig.new
  end

  # GET /remote_configs/1/edit
  def edit
    @sub_title = "Editing Remote Config"
  end

  # POST /remote_configs
  # POST /remote_configs.json
  def create
  end

  # PATCH/PUT /remote_configs/1
  # PATCH/PUT /remote_configs/1.json
  def update
    # raise params.inspect
    respond_to do |format|
      if @remote_config.update(remote_config_params)
        params[:remote_config][:remote_config_android_uis_attributes].each do |key, value|

          id = params[:remote_config][:remote_config_android_uis_attributes][key][:android_ui_attributes][:id]
          grid_type_id = params[:remote_config][:remote_config_android_uis_attributes][key][:android_ui_attributes][:grid_type_id]

          android_ui = AndroidUi.find(id)
          android_ui.grid_type_id = grid_type_id
          android_ui.save

        end

        format.html {redirect_to @remote_config, notice: 'Remote Config was successfully updated.'}
        format.json {render :show, status: :ok, location: @remote_config}
      else
        format.html {render :edit}
        format.json {render json: @remote_config.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /remote_configs/1
  # DELETE /remote_configs/1.json
  def destroy
    @remote_config.destroy
    respond_to do |format|
      format.html {redirect_to remote_configs_url, notice: 'Remote Config was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_remote_config
    @remote_config = RemoteConfig.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def remote_config_params
    params.require(:remote_config).permit(:name)
  end
end
