class RadiosController < ApplicationController
  before_action {set_titles("General Settings", "Radios")}
  before_action :set_radio, only: [:show, :edit, :update, :destroy]

  # GET /radios
  # GET /radios.json
  def index
    @sub_title = "All Radios"
    @new_link = view_context.link_to "New Radio", new_radio_path, class: "dropdown-item"
    @radios = Radio.all
  end

  # GET /radios/1
  # GET /radios/1.json
  def show
  end

  # GET /radios/new
  def new
    @sub_title = "New Radio"
    @radio = Radio.new
  end

  # GET /radios/1/edit
  def edit
    @sub_title = "Editing Radio"
  end

  # POST /radios
  # POST /radios.json
  def create
    @radio = Radio.new(radio_params)

    respond_to do |format|

      uploaded_io = params[:radio][:img]

      if uploaded_io
        file_name = SecureRandom.uuid
        @radio.img = "/uploads/radios/#{file_name}"
      end

      if @radio.save

        if uploaded_io
          upload_file("uploads/radios", file_name, uploaded_io)
        end

        format.html {
          flash[:success] = 'Radio was successfully  created.'
          redirect_to @radio
        }
        format.json {render :show, status: :created, location: @radio}
      else
        format.html {render :new}
        format.json {render json: @radio.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /radios/1
  # PATCH/PUT /radios/1.json
  def update
    respond_to do |format|

      uploaded_io = params[:radio][:img]
      if uploaded_io
        file_name = SecureRandom.uuid
        @radio.img = "/uploads/radios/#{file_name}"
      end

      if @radio.update(radio_params)

        if uploaded_io
          upload_file("uploads/radios", file_name, uploaded_io)
        end

        format.html {
          flash[:success] = 'Radio was successfully  updated.'
          redirect_to @radio
        }
        format.json {render :show, status: :ok, location: @radio}
      else
        format.html {render :edit}
        format.json {render json: @radio.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /radios/1
  # DELETE /radios/1.json
  def destroy
    @radio.destroy
    respond_to do |format|
      format.html {redirect_to radios_url, notice: 'Radio was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_radio
    @radio = Radio.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def radio_params
    params.require(:radio).permit(:name, :genre_id, :tags, :bitrate, :type_radio, :source_radio, :link_radio, :user_agent_radio,
                                  :url_facebook, :url_twitter, :url_instagram, :url_website, :is_featured, :is_active)
  end
end
