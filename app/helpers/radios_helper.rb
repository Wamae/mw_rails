module RadiosHelper
  def render_genres(genre = nil)
    genres = Genre.all

    if genre
      options = {:selected => genre.id}
    else
      options = {:prompt => "Select genre"}
    end

    select("radio", "genre_id", genres.collect {|genre| [genre.name, genre.id]}, options, :class => "form-control")
  end
end
