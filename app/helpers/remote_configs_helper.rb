module RemoteConfigsHelper

  def render_grid_types(selected_grid_type_id = nil)
    # grid_types = GridType.where.not(id: except)
    grid_types = GridType.all

    if selected_grid_type_id
      options = {selected: selected_grid_type_id, class: "form-control"}
    else
      options = {prompt: "Select grid type", class: "form-control"}
    end
    select_tag("remote_config[grid_type_ids[]]", options_from_collection_for_select(grid_types, "id", "name"), options)
  end

  def render_player_types(selected_disc_type = nil)
    disc_types = DiscType.all

    if selected_disc_type
      options = {:selected => selected_disc_type.id}
    else
      options = {:prompt => "Select disc type"}
    end
    select("remote_config", "disc_type_id", disc_types.collect {|disc_type| [disc_type.name, disc_type.id]}, options, :class => "form-control")
  end

  def render_background_mode(selected_bg_mode_id = nil)
    bg_modes = BackgroundMode.all

    if selected_bg_mode_id
      options = {:selected => selected_bg_mode_id}
    else
      options = {:prompt => "Select background mode"}
    end
    select("remote_config", "background_mode_id", bg_modes.collect {|background_mode| [background_mode.name, background_mode.id]}, options, :class => "form-control")
  end

  def render_app_type(selected_app_type_id = nil)
    app_types = AppType.all

    if selected_app_type_id
      options = {:selected => selected_app_type_id}
    else
      options = {:prompt => "Select app type"}
    end
    select("remote_config", "app_type_id", app_types.collect {|app_type| [app_type.name, app_type.id]}, options, :class => "form-control")
  end


end
