Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :radios
  resources :genres

  resources :app_settings
  resources :remote_configs
  root to: 'radios#index'

  namespace :api do
    namespace :private do
      namespace :v1 do
        post 'auth/sign_in' => 'auth#sign_in'
        post 'auth/sign_out' => 'auth#sign_out'

        get 'genres' => 'genres#index'

        get 'radios' => 'radios#index'
        get 'search_by_name' => 'radios#search_by_name'
        get 'radios/:id' => 'radios#show'

        get 'remote_configs' => 'remote_configs#index'
      end
    end
  end
end
